import numpy as np
import cv2
import matplotlib.pyplot as plt

img = cv2.imread('./preprocessing/gabor.png')

# Structuring element
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(30,30))

# Approach-1: Perform erosion and dilation separately and then subtract
erosion = cv2.erode(img, kernel, iterations = 1)
dilation = cv2.dilate(img, kernel, iterations=1)
gradient1 = dilation - erosion

# Approach-2: Use cv2.morphologyEx()
img = cv2.morphologyEx(img, cv2.MORPH_BLACKHAT, kernel)

# ret, img = cv2.threshold(gradient,127,255,cv2.THRESH_BINARY)

cv2.imwrite('./preprocessing/gradient.png',img) 