import cv2
import matplotlib.pyplot as plt
import numpy as np
from skimage.data import camera
from skimage.filters import frangi, hessian

# read image
src = cv2.imread('./dataset/ddb1_fundusimages/image011.png', cv2.IMREAD_UNCHANGED)

# extract green channel
green_channel = src[:,:,1]
# write green channel to greyscale image
cv2.imwrite('./preprocessing/test/extractg.png',green_channel) 


# CLAHE
gimg = cv2.imread('./preprocessing/test/extractg.png')

image_bw = cv2.cvtColor(gimg, cv2.COLOR_BGR2GRAY) 
clahe = cv2.createCLAHE(clipLimit=5)
img = clahe.apply(image_bw)

cv2.imwrite('./preprocessing/test/clahe.png',img) 

img = cv2.imread('./preprocessing/test/clahe.png')

# Gabor

# h, w, _ = img.shape

# ksizeh = h # pixel
# ksizew = w
# sigma = 1.5 # standard deviation
# theta = 45 # angle of rotation 45
# lamda = 10.25
# gamma = 4 # aspect radio
# phi = 0.8 # phase offset

# kernel = cv2.getGaborKernel((ksizeh,ksizew),sigma,theta,lamda,gamma,phi,ktype=cv2.CV_32F)

# fimg = cv2.filter2D(img, cv2.CV_8UC3, kernel)
# # kernel_resized = cv2.resize(kernel,(400,400))
# cv2.imwrite('./preprocessing/test/gabor.png',fimg) 

# Hessian based enhancement filte
# hessian(fimg)
# plt.imshow(fimg)
# plt.show()