import numpy as np
import cv2
import matplotlib.pyplot as plt
from PIL import Image
# from skimage.morphology import thin
import scipy.signal

img = cv2.imread('../preprocessing/test/gabor.png')

# Structuring element
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))

# Approach-1: Perform erosion and dilation separately and then subtract

# opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
erosion = cv2.erode(img, kernel, iterations = 1)
dilation = cv2.dilate(erosion,kernel,iterations = 1)
img =  dilation - erosion
# Denoising
# median = cv2.medianBlur(img,ksize = 3)

# a = cv2.morphologyEx(median, cv2.MORPH_GRADIENT, kernel)
# Approach-2: Use cv2.morphologyEx()

# plt.subplot(2,2,1)
# plt.title("opening")
# plt.imshow(opening)
# plt.subplot(2,2,2)
# plt.title("closing")
# plt.imshow(closing)
# plt.subplot(2,2,3)
# plt.title("erosion")
# plt.imshow(erosion)
# plt.subplot(2,2,4)
# plt.title("dilation")
# plt.imshow(dilation)

# kernel = np.ones((5,5),np.uint8)

# thined = thin(np.array(dilation))

# plt.imshow(gradient1)
cv2.imwrite('../preprocessing/test/dilation.png',dilation)
# cv2.imwrite('../preprocessing/test/opening.png',opening)
# cv2.imwrite('../preprocessing/test/closing.png',closing)
# kernel = np.ones((5, 5), np.uint8) 
# opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel) 
# ret, img = cv2.threshold(gradient,127,255,cv2.THRESH_BINARY)
# ret, img = cv2.threshold(dilation,127,255,cv2.THRESH_BINARY_INV)
# cv2.imwrite('../preprocessing/test/threshold.png',img)

# dst = cv2.fastNlMeansDenoisingColored(img,None,10,7,7,21)

# f, axarr = plt.subplots(1,2)

# axarr[0].imshow(median,'gray')
# axarr[1].imshow(test,'gray')

# plt.imshow(dst,'gray')
# plt.subplot(2,2,1)
# plt.title("Original")
# plt.imshow(img,'gray')
# plt.subplot(2,2,2)
# plt.title("Denoising")
# plt.show()

# cv2.imwrite('../preprocessing/test/thresh.png',img)

# num = 1
# for thre in np.arange(60, 100, 0.5):
#     retval, threshold = cv2.threshold(dilation, thre, 255, cv2.THRESH_BINARY_INV)
#     cv2.imwrite('../preprocessing/test/test/'+str(num)+'.png',threshold) 
#     num += 1

# plt.imshow(dilation)
# plt.show()