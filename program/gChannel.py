import cv2
import matplotlib.pyplot as plt
import numpy as np
 
#read image
src = cv2.imread('./dataset/NM/dataNormal.jpg', cv2.IMREAD_UNCHANGED)
print(src.shape)

#extract green channel
green_channel = src[:,:,1]

#write green channel to greyscale image
cv2.imwrite('./preprocessing/NM/test2.png',green_channel) 

#write green channel to greyscale image
# plt.imshow(green_channel)
plt.show()