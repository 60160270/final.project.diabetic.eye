import numpy as np
import matplotlib.pyplot as plt
import cv2

img = cv2.imread('../preprocessing/test/meijering.png')
ret,thresh1 = cv2.threshold(img,34,255,cv2.THRESH_BINARY)
ret,thresh2 = cv2.threshold(img,127,255,cv2.THRESH_BINARY_INV)
ret,thresh3 = cv2.threshold(img,105,255,cv2.THRESH_TRUNC)
ret,thresh4 = cv2.threshold(img,104,255,cv2.THRESH_TOZERO)
ret,thresh5 = cv2.threshold(img,105,255,cv2.THRESH_TOZERO_INV)

titles = ['Original Image','BINARY','BINARY_INV','TRUNC','TOZERO','TOZERO_INV']
images = [img, thresh1, thresh2, thresh3, thresh4, thresh5]

plt.imshow(images[1])
# cv2.imwrite('../preprocessing/test/th/thresh.png',thresh2)

# for thresh in np.arange(105,121,0.1):
#     ret,thresh2 = cv2.threshold(img,thresh,255,cv2.THRESH_BINARY_INV)
#     cv2.imwrite('../preprocessing/test/test/'+thresh+'.png',thresh2)

plt.show()


# import cv2
# import numpy as np
# import matplotlib.pyplot as plt

# img = cv2.imread('../preprocessing/test/opening.png',0)
# img = cv2.medianBlur(img,5)

# ret,th1 = cv2.threshold(img,150,255,cv2.THRESH_BINARY_INV)
# ret,th2 = cv2.threshold(img,160,255,cv2.THRESH_BINARY_INV)
# ret,th3 = cv2.threshold(img,170,255,cv2.THRESH_BINARY_INV)
# # th2 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY_INV,11,2)
# # th3 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,11,2)

# titles = ['Original Image', 'Global Thresholding (v = 127)','Adaptive Mean Thresholding', 'Adaptive Gaussian Thresholding']
# images = [img, th1, th2, th3]

# for i in range(4):
#     plt.subplot(2,2,i+1),plt.imshow(images[i],'gray')
#     plt.title(titles[i])
#     plt.xticks([]),plt.yticks([])
# plt.show()