import numpy as np
import matplotlib.pyplot as plt
import cv2

img = cv2.imread('../database/healthy/01_h.jpg')
hist,bins = np.histogram(img,256,[0,256])
print(hist)
plt.hist(img.ravel(),256,[0,256])
plt.title('Histogram for gray scale picture')
plt.show()