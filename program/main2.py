import cv2
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import math

# img = Image.open('../dataset/NM/dataNormal.png')
# green_channel.save('../preprocessing/test/extractG.png')

# read image
src = cv2.imread('../database/healthy/01_h.jpg', cv2.IMREAD_UNCHANGED)

# extract green channel
green_channel = src[:,:,1]
# write green channel to greyscale image
cv2.imwrite('../preprocessing/test/extractg.png',green_channel) 

# Start of CLAHE
gimg = cv2.imread('../preprocessing/test/extractg.png')

image_bw = cv2.cvtColor(gimg, cv2.COLOR_BGR2GRAY) 
clahe = cv2.createCLAHE(clipLimit=5)
img = clahe.apply(image_bw)

cv2.imwrite('../preprocessing/test/clahe.png',img) 

# End of CLAHE

# Start of Image Sharpening
# simg = cv2.imread('../preprocessing/test/clahe.png')
# filter = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])

# sharp = cv2.filter2D(simg,-1,filter)
# cv2.imwrite('../preprocessing/test/sharp.png',sharp)
# End of Image Sharpening

# Start of Gabor
img = cv2.imread('../preprocessing/test/clahe.png')

# Median filter
# median = cv2.medianBlur(img, 5)
h, w, _ = img.shape

ksizeh = h # pixel
ksizew = w
sigma = 2 # standard deviation
theta = 45 # angle of rotation 45
lamda = 9.2
gamma = 4 # aspect radio
phi = 0.8 # phase offset

kernel = cv2.getGaborKernel((ksizeh,ksizew),sigma,theta,lamda,gamma,phi,ktype=cv2.CV_32F)

# array = []

# for x in range(ksizeh):
#     row = []
#     for y in range(ksizew):
#         xp = (x*math.cos(theta))+(y*math.sin(theta))
#         yp = (y*math.cos(theta))-(x*math.sin(theta))
#         a = math.exp(-((math.pow(xp,2)+((math.pow(gamma,2))*(math.pow(yp,2))))/2*(math.pow(sigma,2))))
#         b = math.cos(2*math.pi*(xp/lamda))
#         row.append(a*b)
#     array.append(row)

fimg = cv2.filter2D(img, cv2.CV_8UC3, kernel)
kernel_resized = cv2.resize(kernel,(400,400))
cv2.imwrite('../preprocessing/test/gabor.png',fimg)

# End of Gabor

# Start of gradient
from skimage.filters import meijering
img = cv2.imread('../preprocessing/test/gabor.png')

kwargs = {'sigmas': [1], 'mode': 'reflect'}
kwargs['black_ridges'] = 1
ming = meijering(img, **kwargs)
plt.imsave('../preprocessing/test/meijering.png',ming)
# End of gradient

# Start of K MEAN

img = cv2.imread('../preprocessing/test/meijering.png')
vectorized = img.reshape((-1,3))
vectorized = np.float32(vectorized)
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
attempts = 10
K = 5

ret,label,center=cv2.kmeans(vectorized,K,None,criteria,attempts,cv2.KMEANS_PP_CENTERS)
center = np.uint8(center)
res = center[label.flatten()]
result_image = res.reshape((img.shape))

cv2.imwrite('../preprocessing/test/kmean.png',result_image)

# Thre

img = cv2.imread('../preprocessing/test/kmean.png')
ret,thresh = cv2.threshold(img,38,255,cv2.THRESH_BINARY)
cv2.imwrite('../preprocessing/test/thresh.png',thresh)

img = cv2.imread('../preprocessing/test/thresh.png')
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(2,2))
opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
cv2.imwrite('../preprocessing/test/closing.png',closing)
#
# End of K MEAN

#
# img = cv2.imread('../preprocessing/test/meijering.png')
# kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
# erosion = cv2.erode(img, kernel, iterations = 1)
# dilation = cv2.dilate(erosion,kernel,iterations = 1)

# cv2.imwrite('../preprocessing/test/dilation.png',dilation)
#