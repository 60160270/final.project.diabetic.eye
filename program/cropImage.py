import cv2
import numpy as np
import sys
from matplotlib import pyplot as plt
import PIL
from PIL import Image
import pandas as pd

# Image input
img = cv2.imread('./dataset/NM/dataNormal.png', 0)

def crop_image_from_gray(img, tol=0):

    if img.ndim == 2:
        # img = img[0:0+1700, 500:500+1750]
        mask = img > tol
        img = img[np.ix_(mask.any(1), mask.any(0))]
        return img
    elif img.ndim == 3:
        gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        mask = gray_img > tol

        check_shape = img[:, :, 0][np.ix_(mask.any(1), mask.any(0))].shape[0]
        if (check_shape == 0):  # image is too dark so that we crop out everything,
            return img  # return original image
        else:
            img1 = img[:, :, 0][np.ix_(mask.any(1), mask.any(0))]
            img2 = img[:, :, 1][np.ix_(mask.any(1), mask.any(0))]
            img3 = img[:, :, 2][np.ix_(mask.any(1), mask.any(0))]
    #         print(img1.shape,img2.shape,img3.shape)
            img = np.stack([img1, img2, img3], axis=-1)
    #         print(img.shape)
        return img


def circle_crop(img, sigmaX):
    img = crop_image_from_gray(img)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    height, width, depth = img.shape

    x = int(width/2)
    y = int(height/2)
    r = np.amin((x, y))

    circle_img = np.zeros((height, width), np.uint8)
    cv2.circle(circle_img, (x, y), int(r), 1, thickness=-1)
    img = cv2.bitwise_and(img, img, mask=circle_img)
    img = crop_image_from_gray(img)
    img = cv2.addWeighted(img, 4, cv2.GaussianBlur(img, (0, 0), sigmaX), -4, 128)
    return img


# CLAHE
# clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
# img = clahe.apply(img)

# Gabor filter
# kernel = cv2.getGaborKernel((ksize, ksize), sigma, theta, lamda, gamma, phi, ktype=cv2.CV_32F)
# img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Hessian based enhancement filter

imgCroped = plt.imshow(circle_crop(img,50), cmap='gray')
# plt.show()
plt.axis('off')

# imgCroped.set_array(imgCroped)

# cv2.imwrite('croped.jng',imgCroped)
plt.savefig('preprocessing/NM/croped2.png')

# Vessel extraction K Mean
# Morphological cleaning
