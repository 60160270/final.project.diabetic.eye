import numpy as np
import cv2
import matplotlib.pyplot as plt
from PIL import Image

img = cv2.imread('./preprocessing/NM/NM1.png')
h, w, _ = img.shape

print(w)

ksize = 15 # pixel
sigma = 1.5 # standard deviation
theta = 1*np.pi/4 # angle of rotation 45
lamda = 1*np.pi/4 # angle of rotation 45
gamma = 4 # aspect radio
phi = 0.8 # phase offset

kernel = cv2.getGaborKernel((ksize,ksize),sigma,theta,lamda,gamma,phi,ktype=cv2.CV_32F)



fimg = cv2.filter2D(img, cv2.CV_8UC3, kernel)
# kernel_resized = cv2.resize(kernel,(400,400))




# cv2.imwrite('./preprocessing/NM/NM1.png',fimg)

plt.imshow(fimg)
# plt.imshow(kernel_resized)
plt.show()