import cv2
import matplotlib.pyplot as plt

# CLAHE
gimg = cv2.imread('./preprocessing/NM/test.png')
image_bw = cv2.cvtColor(gimg, cv2.COLOR_BGR2GRAY) 

clahe = cv2.createCLAHE(clipLimit=5)
img = clahe.apply(image_bw)

plt.imshow(img)
plt.show()
cv2.imwrite('./preprocessing/NM/test.png',img) 