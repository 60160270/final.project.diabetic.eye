import numpy as np
import cv2
import matplotlib.pyplot as plt

img = cv2.imread('../preprocessing/test/thresh.png')

median = cv2.medianBlur(img, 5)
# compare = np.concatenate((img, median), axis=1) #side by side comparison

cv2.imwrite('../preprocessing/test/median.png',median)
