import numpy as np
import matplotlib.pyplot as plt
import cv2

img = cv2.imread('../preprocessing/test/meijering.png')

# Start of K MEAN using K = 8
vectorized = img.reshape((-1,3))
vectorized = np.float32(vectorized)
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
attempts = 10
K = 8

num = 1
for K in np.arange(1,8,1):
    ret,label,center=cv2.kmeans(vectorized,K,None,criteria,attempts,cv2.KMEANS_PP_CENTERS)
    center = np.uint8(center)
    res = center[label.flatten()]
    result_image = res.reshape((img.shape))
    figure_size = 15
    cv2.imwrite('../kmean/'+str(num)+'.png',result_image)
    num += 1

# ret,label,center=cv2.kmeans(vectorized,K,None,criteria,attempts,cv2.KMEANS_PP_CENTERS)
# center = np.uint8(center)
# res = center[label.flatten()]
# result_image = res.reshape((img.shape))

# cv2.imwrite('../preprocessing/test/kmean.png',result_image)
# End of K Mean