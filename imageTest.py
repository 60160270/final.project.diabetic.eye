import cv2
import matplotlib.pyplot as plt

img = cv2.imread('./preprocessing/gabor.png')
ret, bw_img = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
plt.imshow(bw_img)
plt.show()