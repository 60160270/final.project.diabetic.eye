from skimage.filters import frangi, hessian
import numpy as np
import matplotlib.pyplot as plt
import cv2

img = cv2.imread('./preprocessing/gabor.png')
img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

# Binary image
himg = cv2.subtract(255, img) 

# K MEAN
# vectorized = himg.reshape((-1,3))
# vectorized = np.float32(vectorized)
# criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
# attempts = 10
# num = 1
# for K in np.arange(1,20,1):
#     ret,label,center=cv2.kmeans(vectorized,K,None,criteria,attempts,cv2.KMEANS_PP_CENTERS)
#     center = np.uint8(center)
#     res = center[label.flatten()]
#     result_image = res.reshape((himg.shape))
#     figure_size = 15
#     cv2.imwrite('./kmean/'+str(num)+'.png',result_image)
#     num += 1
# himg = result_image

plt.imshow(himg, cmap = "gray")
plt.show()