import numpy as np
import cv2
import matplotlib.pyplot as plt
import pandas as pd

# ksize = 9  #Use size that makes sense to the image and fetaure size. Large may not be good.
# #On the synthetic image it is clear how ksize affects imgae (try 5 and 50)
# sigma = 8 #Large sigma on small features will fully miss the features.
# theta = 0  #/4 shows horizontal 3/4 shows other horizontal. Try other contributions
# lamda = 5  #1/4 works best for angled.
# gamma = 0.5  #Value of 1 defines spherical. Calue close to 0 has high aspect ratio
# #Value of 1, spherical may not be ideal as it picks up features from other regions.
# phi = 0  #Phase offset. I leave it to 0. (For hidden pic use 0.8)

# # plt.imshow(kernel)

#img = cv2.imread('images/synthetic.jpg')
# img = cv2.imread('images/zebra.jpg')  #Image source wikipedia: https://en.wikipedia.org/wiki/Plains_zebra
# USe ksize:15, s:5, q:pi/2, l:pi/4, g:0.9, phi:0.8
img = cv2.imread('./preprocessing/clahe.png')
h, w, _ = img.shape

img2 = img.reshape(-1)
df = pd.DataFrame()
df['Original'] = img2

# kernel = cv2.getGaborKernel((ksize, ksize), sigma, theta, lamda, gamma, phi, ktype=cv2.CV_32F)


ksizeh = h  # pixel
ksizew = w
# sigma = 1.5 # standard deviation
# theta = 45 # angle of rotation 45
# lamda = 10
# gamma = 4 # aspect radio
# phi = 0.8 # phase offset

num = 1
kernels = []
for lamda in np.arange(8, 12, 0.1):
    gabor_label = 'Gabor' + str(num)
    sigma = 1 # standard deviation
    theta = 30 # angle of rotation 45
    gamma = 0.5 # aspect radio
    phi = 0.8 # phase offset
    kernel = cv2.getGaborKernel(
        (ksizeh, ksizew), sigma, theta, lamda, gamma, phi, ktype=cv2.CV_32F)
    kernels.append(kernel)

    fimg = cv2.filter2D(img2, cv2.CV_8UC3, kernel)
    filtered_img = fimg.reshape(-1)

    cv2.imwrite('./gabor/output'+gabor_label+'.jpg',
                filtered_img.reshape(img.shape))

    df[gabor_label] = filtered_img

    num += 1

# fimg = cv2.filter2D(img, cv2.CV_8UC3, kernel)

# kernel_resized = cv2.resize(kernel, (400, 400))                    # Resize image


# plt.imshow(kernel_resized)
# plt.imshow(img)
# plt.show()

#cv2.imshow('Kernel', kernel_resized)
#cv2.imshow('Original Img.', img)
#cv2.imshow('Filtered', fimg)
# cv2.waitKey()
# cv2.destroyAllWindows()
